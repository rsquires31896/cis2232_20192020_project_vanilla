package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.CourtTimes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author arman
 */

@Repository
public interface CourtTimesRepository extends CrudRepository<CourtTimes, String> {


}