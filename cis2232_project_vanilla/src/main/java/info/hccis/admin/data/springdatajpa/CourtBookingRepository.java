package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.CourtBooking;
import info.hccis.admin.model.jpa.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author arman
 */

@Repository
public interface CourtBookingRepository extends CrudRepository<CourtBooking, Integer> {


}