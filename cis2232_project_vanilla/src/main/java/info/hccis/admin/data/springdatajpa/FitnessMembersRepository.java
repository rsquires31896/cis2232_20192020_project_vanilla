/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.FitnessMember;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author  asheikho
 * @since   20191123
 * 
 */
@Repository
public interface FitnessMembersRepository extends CrudRepository<FitnessMember, Integer> {
    
}
