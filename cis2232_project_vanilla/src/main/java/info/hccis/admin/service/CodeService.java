package info.hccis.admin.service;

import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.model.jpa.CodeType;
import java.util.ArrayList;


/**
 * Code service 
 *
 * @author BJ MacLean
 */
public interface CodeService {

    public abstract CodeTypeRepository getCtr();
    public abstract UserRepository getUr();
    
    /**
     *
     * @param codeTypeId
     * @return
     */
    public abstract ArrayList<CodeType> getCodeTypes();
    
    /**
     *
     * @param codeTypeId
     * @return
     */
    public abstract ArrayList<CodeType> getCodeTypes(DatabaseConnection databaseConnection);

    /**
     *
     * @param codeTypeId
     * @return
     */
    public abstract ArrayList<User> getUsers();

    
}
