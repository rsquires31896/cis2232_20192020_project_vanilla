/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.entity;

/**
 *
 * @author rsquires31896
 */
public class AvailableCourt {
    
    private Integer id;
    private int courtNumber;
    private String courtName;
    private int courtType;
    private String codeDescription;
    private String startTime;
    private String endTime;
    private String dateBooked;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the courtNumber
     */
    public int getCourtNumber() {
        return courtNumber;
    }

    /**
     * @param courtNumber the courtNumber to set
     */
    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    /**
     * @return the courtName
     */
    public String getCourtName() {
        return courtName;
    }

    /**
     * @param courtName the courtName to set
     */
    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    /**
     * @return the codeDescription
     */
    public String getCodeDescription() {
        return codeDescription;
    }

    /**
     * @param codeDescription the codeDescription to set
     */
    public void setCodeDescription(String codeDescription) {
        this.codeDescription = codeDescription;
    }

    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the courtType
     */
    public int getCourtType() {
        return courtType;
    }

    /**
     * @param courtType the courtType to set
     */
    public void setCourtType(int courtType) {
        this.courtType = courtType;
    }

    /**
     * @return the dateBooked
     */
    public String getDateBooked() {
        return dateBooked;
    }

    /**
     * @param dateBooked the dateBooked to set
     */
    public void setDateBooked(String dateBooked) {
        this.dateBooked = dateBooked;
    }
    
}
