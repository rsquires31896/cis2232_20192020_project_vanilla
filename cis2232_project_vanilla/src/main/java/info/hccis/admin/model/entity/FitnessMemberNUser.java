/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author arman
 */
@Entity
public class FitnessMemberNUser implements Serializable {

    private static final long serialVersionUID = 1L;
  
    @Id
    private int userId;
    private int fitnessMemberId;
    private String name;

    public FitnessMemberNUser(){
        
    }
    
    public FitnessMemberNUser(int userId, int fitnessMemberId, String firstName, String lastName){
        this.fitnessMemberId = fitnessMemberId;
        this.name = lastName + "," + firstName;
        this.userId = userId;
    }
    
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFitnessMemberId() {
        return fitnessMemberId;
    }

    public void setFitnessMemberId(int fitnessMemberId) {
        this.fitnessMemberId = fitnessMemberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    
}
