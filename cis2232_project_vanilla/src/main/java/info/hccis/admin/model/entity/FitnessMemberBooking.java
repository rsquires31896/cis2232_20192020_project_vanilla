/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.entity;

/**
 *
 * @author LEZOMY
 */
public class FitnessMemberBooking {
    
    // FitnessMember Table
    private int userId;
    
    // UserAccess Table
    private String firstName;
    private String lastName;
    private String username;
    
    // CourtBooking Table
    private int courtNumber;
    private String bookingDate;
    private String startTime;
    private int memberId;
    private int memberIdOpponent;
    private String notes;
    
    private String fullName;
    private String startDate;
    private String endDate;
    
    public FitnessMemberBooking(){
    }
    
    public FitnessMemberBooking(String fullName, int courtNumber, int memberId, int memberIdOpponent, String startTime, String notes){
        this.fullName = fullName;
        this.courtNumber = courtNumber;
        this.memberId = memberId;
        this.memberIdOpponent = memberIdOpponent;
        this.startTime = startTime;
        this.notes = notes;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the courtNumber
     */
    public int getCourtNumber() {
        return courtNumber;
    }

    /**
     * @param courtNumber the courtNumber to set
     */
    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    /**
     * @return the bookingDate
     */
    public String getBookingDate() {
        return bookingDate;
    }

    /**
     * @param bookingDate the bookingDate to set
     */
    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the memberIdOpponent
     */
    public int getMemberIdOpponent() {
        return memberIdOpponent;
    }

    /**
     * @param memberIdOpponent the memberIdOpponent to set
     */
    public void setMemberIdOpponent(int memberIdOpponent) {
        this.memberIdOpponent = memberIdOpponent;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @author      asheikho
     * @since       20193011
     * @description concatenate firstName and lastName
     * @return      the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the memberId
     */
    public int getMemberId() {
        return memberId;
    }

    /**
     * @param memberId the memberId to set
     */
    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    
}
