/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
//import javax.persistence.Id;

/**
 *
 * @author arman
 */

public class MemberUsage implements Serializable {

    private static final long serialVersionUID = 1L;
    private String memberName;
    private int noOfBookings;
    private String startDate, endDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public int getNoOfBookings() {
        return noOfBookings;
    }

    public void setNoOfBookings(int noOfBookings) {
        this.noOfBookings = noOfBookings;
    }
    
    
    @Override
    public String toString() {
        return "info.hccis.admin.model.entity.MemberUsage[ memberName=" + memberName 
                + "; noOfBookings=" + noOfBookings 
                + "; startDate=" + startDate 
                + "; endDate=" + endDate 
                + " ]";
    }
    
}
