/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author arman
 */



@Entity
@Table(name = "CourtBooking")
@NamedQueries({
    @NamedQuery(name = "CourtBooking.findAll", query = "SELECT c FROM CourtBooking c")})
@XmlRootElement
public class CourtBooking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtNumber")
    private int courtNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "bookingDate")
    private String bookingDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "startTime")
    private String startTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "memberId")
    private int memberId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "memberIdOpponent")
    private int memberIdOpponent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "notes")
    private String notes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "createdDate")
    private String createdDate;

    
    @Transient
    private String courtName, memberName, opponentName;

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }


    public CourtBooking() {
    }

    public CourtBooking(Integer id) {
        this.id = id;
    }

    public CourtBooking(Integer id, int courtNumber, String bookingDate, String startTime, int memberId, int memberIdOpponent, String notes, String createdDate) {
        this.id = id;
        this.courtNumber = courtNumber;
        this.bookingDate = bookingDate;
        this.startTime = startTime;
        this.memberId = memberId;
        this.memberIdOpponent = memberIdOpponent;
        this.notes = notes;
        this.createdDate = createdDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCourtNumber() {
        return courtNumber;
    }

    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getMemberIdOpponent() {
        return memberIdOpponent;
    }

    public void setMemberIdOpponent(int memberIdOpponent) {
        this.memberIdOpponent = memberIdOpponent;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CourtBooking)) {
            return false;
        }
        CourtBooking other = (CourtBooking) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.CourtBooking[ id=" + id
                + "; courtNumber=" + courtNumber 
                + "; bookingDate=" + bookingDate 
                + "; courtName=" + courtName 
                + "; memberId=" + memberId 
                + "; memberName=" + memberName 
                + "; memberIdOpponent=" + memberIdOpponent 
                + "; opponentName=" + opponentName
                + "; notes=" + notes
                + "; createdDate=" + createdDate
                +" ]";
    }
    
}
