/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.bo;

import com.google.gson.Gson;
import info.hccis.admin.dao.CourtDAO;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.model.entity.AvailableCourt;
import info.hccis.admin.model.jpa.Court;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author rsquires31896
 */
public class CourtBO {
    public static AvailableCourt currentCriteria;
    public static final String PATH = "c:/fitness/";
    static SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_hhmmss");  
    CourtRepository cr = null;
    static CodeValueRepository cvr;
    
    public CourtBO(CourtRepository cr){
        this.cr = cr;
    }
    
    public Court getCourt(int id){
        return cr.findOne(id);
    }
    
    public static void writeCourtsToFile(ArrayList<Court> courts) throws IOException{
        try { 
            Files.createDirectories(Paths.get(PATH));

            //Get Current Date
            Date date = new Date();
            //Create File
            String fileName = PATH+"court_"+formatter.format(date)+".json";
            File file = new File(fileName);
            file.createNewFile();
           
            //Resets JSON String to blank everytime the file is written as an extra precaution.
            String jsonString = "";
            //Loop through all array objects and concatenate to a string to write
            //to file
            for(Court current : courts){
                Gson gson = new Gson();
                jsonString += gson.toJson(current)+"\n";
            }
            try ( 
                //Write to File
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
                writer.write(jsonString);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }
    }
    
    
    
    public static ArrayList<AvailableCourt> findBookableCourts(int courtType, String startTime, String endTime, String date){
        //Place Holder for all courts
        ArrayList<AvailableCourt> availableCourts = new ArrayList<AvailableCourt>();
        
        //Find all possible Courts
        ArrayList<AvailableCourt> courts = CourtDAO.findByTypeAndTime(courtType, startTime, endTime);
        
        //Find all booked courts
        ArrayList<AvailableCourt> bookedCourts = CourtDAO.findByTypeTimeAndDate(courtType, startTime, endTime, date);
        
        //Loop through courts and removed bookings
        for(AvailableCourt current : courts){
            boolean isBooked = false;
            
            for(AvailableCourt booking : bookedCourts){                             
                if(current.getStartTime().equals(booking.getStartTime()) && current.getCourtName().equals(booking.getCourtName())){
                    isBooked = true;
                }                       
            }
            
            if(!isBooked){
                availableCourts.add(current);
            }
        }
        //Filter out booked courts
        
        return availableCourts;
    }
    
    public static AvailableCourt ValidateCriteria(AvailableCourt acCriteria){
        if(acCriteria == null){
            acCriteria = new AvailableCourt();
        }
        
        return acCriteria;
    }
    
    
}
