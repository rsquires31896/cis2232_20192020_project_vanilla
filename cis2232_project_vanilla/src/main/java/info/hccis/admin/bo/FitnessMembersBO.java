/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.bo;

import com.google.gson.Gson;
import info.hccis.admin.model.jpa.FitnessMember;
import info.hccis.admin.model.entity.FitnessMemberBooking;
import info.hccis.admin.data.springdatajpa.FitnessMembersRepository;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author asheikho
 * @since 20191101
 * @description Writes Fitness members to a file in local drive
 */
public class FitnessMembersBO {

    public static final String PATH = "c:/fitness/";
    static SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_hhmmss");
    FitnessMembersRepository fmr = null;

    public FitnessMembersBO(FitnessMembersRepository fmr) {
        this.fmr = fmr;
    }

    public FitnessMember getFitnessMember(int id) {
        return fmr.findOne(id);
    }

    public static void writeFitnessMembersToFile(ArrayList<FitnessMember> fitnessMembers) throws IOException {

        try {
            Files.createDirectories(Paths.get(PATH));

            //Get Current Date
            Date date = new Date();
            //Create File
            String fileName = PATH + "member_" + formatter.format(date) + ".json";
            File file = new File(fileName);
            file.createNewFile();

            //Resets JSON String to blank everytime the file is written as an extra precaution.
            String jsonString = "";
            //Loop through all array objects and concatenate to a string to write
            //to file
            for (FitnessMember current : fitnessMembers) {
                Gson gson = new Gson();
                jsonString += gson.toJson(current) + "\n";
            }
            try (
                    //Write to File
                    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
                writer.write(jsonString);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }

    }
}
