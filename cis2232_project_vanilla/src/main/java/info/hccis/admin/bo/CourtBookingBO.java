/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.bo;


import info.hccis.admin.data.springdatajpa.CourtBookingRepository;
import info.hccis.admin.data.springdatajpa.CourtTimesRepository;
import info.hccis.admin.model.jpa.CourtBooking;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
/**
 *
 * @author aalfonso
 */
public class CourtBookingBO {
    public static final String PATH = "c:/fitness/";
    static SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_hhmmss");  
    CourtBookingRepository cbr = null;
    CourtTimesRepository ctr = null;
    int maxAllowedBooking = 2;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    

    public CourtBookingBO(CourtBookingRepository cr){
        this.cbr = cr;
    }
    
    /**
     * This will retrieve the courtBooking record using id
     * 
     * @param id this is the id of the courtBooking record
     * @return court booking record for the id
     */
    public CourtBooking getCourtBooking(int id){
        return cbr.findOne(id);
    }
    
    /**
     * Will return an array list for all courtBooking records
     * @return all court bookings in array list
     */
    public ArrayList<CourtBooking> getAllCourtBookings(){
        return (ArrayList<CourtBooking>) cbr.findAll();
    }
 
}
