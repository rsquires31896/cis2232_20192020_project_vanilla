package info.hccis.admin.util;

import info.hccis.admin.model.jpa.User;
import javax.servlet.http.HttpSession;

/**
 * This class will provide security utility methods
 *
 * @author bjmaclean
 * @since 20170530
 */
public class UtilSecurity {

    public static boolean validUser(HttpSession session) {
        User user = (User) session.getAttribute("loggedIn");
        try {
            if (user == null || user.getUserId() == 0) {
                System.out.println("Invalid user");
                return false;
            } else {
                System.out.println("User OK");
                return true;
            }
        } catch (Exception e) {
            return false;
        }

    }
}