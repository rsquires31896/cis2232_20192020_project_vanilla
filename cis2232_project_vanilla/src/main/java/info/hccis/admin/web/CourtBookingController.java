package info.hccis.admin.web;

import info.hccis.admin.bo.CourtBookingBO;
import info.hccis.admin.dao.CourtBookingDAO;
import info.hccis.admin.dao.CourtDAO;
import info.hccis.admin.dao.CourtTimesDAO;
import info.hccis.admin.data.springdatajpa.CourtBookingRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.entity.FitnessMemberNUser;
import info.hccis.admin.model.entity.MemberUsage;
import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.model.jpa.CourtBooking;
import info.hccis.admin.model.jpa.CourtTimes;
import info.hccis.admin.util.Utility;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.codehaus.jackson.map.AnnotationIntrospector.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
public class CourtBookingController {
    

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    private final CourtBookingRepository cbr;
    private final CourtRepository cr;
    private final UserRepository ur;
    private final static int MAXBOOKINGCOUNT = 2;
    
    @Autowired
    public CourtBookingController(CourtBookingRepository cbr, CourtRepository cr, UserRepository ur){
        this.cbr = cbr;
        this.cr = cr;
        this.ur = ur;
    }
   
     
    @RequestMapping("/courtbookings/export")
    public String showCourtBookings(Model model, HttpSession session) throws IOException {
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");

        //get CourtBookings
        ArrayList<CourtBooking> courtBookings = CourtBookingDAO.getCourtBookings(databaseConnection);

        //folder location
        final String PATH = "c:/fitness/";
        Files.createDirectories(Paths.get(PATH));
        //filename to save the results
        String fileName = PATH + "courts_" + Utility.getNow("yyyyMMdd") + ".json";
        
        //write list to file
        CourtBookingDAO.writeListToFile(courtBookings, fileName);
        return "courtbookings/export";
    }

    @RequestMapping("/courtbookings/add")
    public String add(Model model, HttpSession session) throws IOException {
        //DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");

        CourtBooking courtBooking = new CourtBooking();
        model.addAttribute ("courtBooking", courtBooking); 
        
        //default values
        courtBooking.setBookingDate(sdf.format(new Date()));
        courtBooking.setCreatedDate(sdf.format(new Date()));
        courtBooking.setId(0);

        //get CourtBookings
        ArrayList<CourtBooking> courtBookings = CourtBookingDAO.getFutureCourtBookings();
        model.addAttribute("courtBookings", courtBookings);
        
        //get courtTimes
        
        ArrayList<CourtTimes> courtTimes = CourtTimesDAO.selectAll();
        model.addAttribute("courtTimes", courtTimes);

        //get courts
  
        ArrayList <Court> courts =  CourtDAO.selectAll();
        model.addAttribute("courts", courts); 
       
        //get members
        ArrayList<FitnessMemberNUser> fitnessMemberNUsers = CourtBookingDAO.getFitnessMemberNUsers();
        System.out.println("fitnessMemberNUsers.size():" + fitnessMemberNUsers.size());
        
        for (int i=0;i<fitnessMemberNUsers.size();i++){
            System.out.println("aa-fitnessMember:" + fitnessMemberNUsers.get(i).getName());
        }
        model.addAttribute("fitnessMemberNUsers", fitnessMemberNUsers);
        


        return "courtbookings/add";
    }

    
    @RequestMapping("/courtbookings/addSubmit")
    public String courtBookingsAddSubmit(Model model, @Valid @ModelAttribute("courtBooking") CourtBooking courtBookingFromTheForm, BindingResult result, HttpSession session) throws Exception {
         System.out.println("aa-msg: result:" + result.toString());

/*
        boolean loggedIn = Utility.verifyLogin(session);
        //Verify that the user is logged in
        if (!loggedIn) {
            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
            model.addAttribute("user", new UserAccess());
            return "other/welcome";
        }
*/      
        this.loadCourtBookingExtraDetail(model, courtBookingFromTheForm);
        this.loadDropDowns(model);
        System.out.println("aa-afterloading:" + courtBookingFromTheForm);
//          
        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            model.addAttribute("message","Error in validation");
            System.out.println("aa-courtBookingFromTheForm.memberId:" + courtBookingFromTheForm.getMemberId());
            return "courtbookings/add";
        }
      
        //validate if member and opponent are the same person.
        if (courtBookingFromTheForm.getMemberId()==courtBookingFromTheForm.getMemberIdOpponent()){
            model.addAttribute("message","Member and Opponent cannot be the same person.");
            return "courtbookings/add";
        }
        // validate date field and bookingDate >= today's date
        
        //this forces the date to remove the time portion
        Date now = new Date();
        String nowDate = sdf.format(now);
        now = sdf.parse(nowDate);
        Date bookingDate;
        try {
            bookingDate = sdf.parse(courtBookingFromTheForm.getBookingDate());
            System.out.println("bookingDate.value:" + bookingDate);
            System.out.println("now.value:" + nowDate);

            if (bookingDate.before(now)){ //bookingDate is before today
                model.addAttribute("message","Error in validation - Booking Date must be greater than or equal today");
                return "courtbookings/add";
            }
        } catch (ParseException e) {
            model.addAttribute("message","Error in validation - Booking Date has invalid format YYYYMMDD");
            System.out.println("aa-there was error on parsing bookingDate");
            return "courtbookings/add";
        }
        
        //validate time is after time now, if bookingDate is today's date
        if (bookingDate.equals(now)){
            
            Calendar cal = Calendar.getInstance();
            Date date=cal.getTime();
            DateFormat dateFormat = new SimpleDateFormat("HHmm");
            String formattedTimeNow=dateFormat.format(date);
            System.out.println("aa-formattedTimeNow:" + formattedTimeNow);
            System.out.println("aa-startTime:" + courtBookingFromTheForm.getStartTime());
            if (Integer.parseInt(formattedTimeNow)>Integer.parseInt(courtBookingFromTheForm.getStartTime())) {
                model.addAttribute("message","TimeStart for today's bookingDate must be after system time:" + formattedTimeNow);
                return "courtbookings/add";
            }
        }
        
        //check if member has exceeded bookings
        System.out.println("aa-checking exceeded bookings");
        int bookingCountForMemberId = CourtBookingDAO.getCountFutureBookingByMember(courtBookingFromTheForm.getMemberId());
        System.out.println("aa-bookingCountForMemberId:" + bookingCountForMemberId);
        if (bookingCountForMemberId>=MAXBOOKINGCOUNT) {
            model.addAttribute("message","Member: " + courtBookingFromTheForm.getMemberName() + " has exceeded max number of bookings");
            
            return "courtbookings/add";  
        }
        
        //check if memberOpponent has exceeded bookings
        int bookingCountForMemberIdOpponent = CourtBookingDAO.getCountFutureBookingByMember(courtBookingFromTheForm.getMemberIdOpponent());
        if (bookingCountForMemberIdOpponent>=MAXBOOKINGCOUNT) {
            model.addAttribute("message","Member: " + courtBookingFromTheForm.getOpponentName()+ " has exceeded max number of bookings");
            return "courtbookings/add";
        }
        
       //verify court is available
        System.out.println("aa-checking court availability");
        if (!CourtBookingDAO.isCourtAvailable(courtBookingFromTheForm.getBookingDate(), 
                courtBookingFromTheForm.getStartTime(),courtBookingFromTheForm.getCourtNumber())){
            model.addAttribute("message", courtBookingFromTheForm.getCourtName() 
                + " is not available on " + courtBookingFromTheForm.getBookingDate()
                + " " + courtBookingFromTheForm.getStartTime());
//            model.addAttribute("courtBooking", courtBookingFromTheForm);
            return "courtbookings/add";
        }
    
        //Call the dao method to put this guy in the database.
        try {
            cbr.save(courtBookingFromTheForm);

        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        // get all courtbookings to list and load it
        this.loadCourtBookings(model);
        //This will send the user to the welcome.html page.
        return "courtbookings/list";
    }
    
    
       @RequestMapping("/courtbookings/delete")
    public String delete(Model model, HttpServletRequest request, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }

        //This will send the user to the welcome.html page.
     
        cbr.delete(Integer.parseInt(request.getParameter("id")));

        //reload CourtBookings
        this.loadCourtBookings(model);
        
        return "courtbookings/list";
    }
@RequestMapping("/courtbookings/list")
    public String list(Model model, HttpServletRequest request, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }

      //reload CourtBookings
        ArrayList<Court> courts  = (ArrayList<Court>) cr.findAll();
        System.out.println("courts.size():"+courts.size());
        this.loadDropDowns(model);
        this.loadCourtBookings(model);
        
        return "courtbookings/list";
    }
    @RequestMapping("/courtbookings/edit")
    public String edit(Model model, HttpServletRequest request, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }

        //This will send the user to the welcome.html page.
        CourtBookingBO cbBO = new CourtBookingBO(cbr);
        CourtBooking courtBooking = cbBO.getCourtBooking(Integer.parseInt(request.getParameter("id")));
        //set createdDate based on today's date
        courtBooking.setCreatedDate(sdf.format(new Date()));
        
        model.addAttribute("courtBooking", courtBooking); 
        loadDropDowns(model);
           
        return "courtbookings/add";
    }
    
    //this will load CourtBooking list with the extra detail
    private void loadCourtBookings(Model model){
//        ArrayList<CourtBooking> courtBookings = CourtBookingDAO.getFutureCourtBookings();
        ArrayList<CourtBooking> courtBookings = CourtBookingDAO.selectAll();
        
        //populate the foreign information for all courtBookings
        for (CourtBooking current : courtBookings) { 
            this.loadCourtBookingExtraDetail(model, current);
        }
        model.addAttribute("courtBookings", courtBookings);
        
    }

    //this will load the values that are not typically in a CourtBooking (ie. Transient)
    private void loadCourtBookingExtraDetail(Model model, CourtBooking courtBooking){
        
        //populate the foreign information
       
        courtBooking.setCourtName(CourtBookingDAO.findCourtNameByCourtNumber(courtBooking.getCourtNumber()));
        courtBooking.setMemberName(CourtBookingDAO.findNameByUserId(courtBooking.getMemberId()));
        courtBooking.setOpponentName(CourtBookingDAO.findNameByUserId(courtBooking.getMemberIdOpponent()));
     
        model.addAttribute("courtBooking", courtBooking);
        
    }

    //this will load the values on the Drop Downs
    public void loadDropDowns(Model model){
            //get CourtBookings
//            ArrayList<CourtBooking> courtBookings = CourtBookingDAO.getFutureCourtBookings();
//            model.addAttribute("courtBookings", courtBookings);

            //get courtTimes
            ArrayList<CourtTimes> courtTimes = CourtTimesDAO.selectAll();
            model.addAttribute("courtTimes", courtTimes);
            
            //get courts
            ArrayList <Court> courts =  CourtDAO.selectAll();
            model.addAttribute("courts", courts); 

            //get members
            ArrayList<FitnessMemberNUser> fitnessMemberNUsers = CourtBookingDAO.getFitnessMemberNUsers();
//            System.out.println("fitnessMemberNUsers.size():" + fitnessMemberNUsers.size());

//            for (int i=0;i<fitnessMemberNUsers.size();i++){
//                System.out.println("aa-fitnessMember:" + fitnessMemberNUsers.get(i).getName());
//            }
            model.addAttribute("fitnessMemberNUsers", fitnessMemberNUsers);
    }
    
    @RequestMapping("/courtbookings/report")
    public String getMemberUsage(Model model,HttpServletRequest request,  HttpSession session) throws IOException {
    
        MemberUsage memberUsage = new MemberUsage(); 
        memberUsage.setStartDate("20190101");
        memberUsage.setEndDate("20190101");
        
        model.addAttribute("memberUsage",memberUsage);
        
        return "courtbookings/report";
        
    
    }
    @RequestMapping("/courtbookings/addReport")
    public String addReport(Model model,HttpServletRequest request,  HttpSession session, @ModelAttribute("memberUsage") MemberUsage memberUsage) throws IOException {
        //get members
        ArrayList<FitnessMemberNUser> fitnessMemberNUsers = CourtBookingDAO.getFitnessMemberNUsers();
   
        ArrayList<MemberUsage> memberUsages = new ArrayList<MemberUsage>();

        for (int i=0;i<fitnessMemberNUsers.size();i++){
            MemberUsage newMemberUsage = new MemberUsage();
            
            //get the name and store in memberUsage record 
            String name = fitnessMemberNUsers.get(i).getName();
            newMemberUsage.setMemberName(name);
            
            //search the count of courtbookings based on name, start and end date
            newMemberUsage.setNoOfBookings(CourtBookingDAO.getBookingsCountByMemberId(
                fitnessMemberNUsers.get(i).getUserId(),
                Integer.parseInt(memberUsage.getStartDate()),
                Integer.parseInt(memberUsage.getEndDate())));
            System.out.println("aa-memberUsage:" +i +":"+ newMemberUsage);
            memberUsages.add(newMemberUsage);
        }

    
//        for (int i=0; i< memberUsages.size(); i++){
//            System.out.println("aa1131-memberUsage:" + memberUsages.get(i));
//        }

        model.addAttribute("memberUsage",memberUsage);
       
        model.addAttribute("memberusages",memberUsages);
        return "courtbookings/report";
    }
    
}
