package info.hccis.admin.web;

import info.hccis.admin.dao.CourtDAO;
//import info.hccis.camper.jpa.Camper;
import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.bo.CourtBO;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.entity.AvailableCourt;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CourtController {

    private final CourtRepository cr;   

    /**
     * This constructor will inject the Repository object into this
     * controller.It will allow this class's methods to use methods from this
     * Spring JPA repository object.
     *
     * @param cr
     * @since 20191021
     * @author BJM
     * @modified by: Robbie Squires
     * @since 20191122
     * Modified to fit Court objects
     */
    @Autowired
    public CourtController(CourtRepository cr) {
        this.cr = cr;
    }
    
    @RequestMapping("/court/export")
    public String exportCourtsToFile() throws IOException{
        
        ArrayList<Court> courts = CourtDAO.selectAll();
        CourtBO.writeCourtsToFile(courts);
        
        return "court/export";
    }
    
    @RequestMapping("/court/add")
    public String add(Model model, HttpSession session) {
//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }

        Court court = new Court();
        model.addAttribute("court", court);
        

        return "court/add";
    }

    @RequestMapping("/court/edit")
    public String edit(Model model, HttpServletRequest request, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }

        CourtBO courtBO = new CourtBO(cr);
        Court court = courtBO.getCourt(Integer.parseInt(request.getParameter("id")));

        model.addAttribute("court", court);

        return "court/add";
    }

    @RequestMapping("/court/list")
    public String list(Model model, HttpServletRequest request, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }

        List<Court> courts = (List<Court>) cr.findAll();
        for (Court current : courts) {
            current.setCourtTypeDescription(CodeValueDAO.getCodeValueDescription(new DatabaseConnection(), 2, current.getCourtType()));
        }
        model.addAttribute("courts", courts);

        return "court/list";
    }
    
    
    @RequestMapping("/court/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("court") Court court, BindingResult result, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);
            return "court/add";
        }

        try {
            Court jpaCourt = new Court();
            jpaCourt.setId(court.getId());
            jpaCourt.setCourtNumber(court.getCourtNumber());
            jpaCourt.setCourtName(court.getCourtName());
            jpaCourt.setCourtType(court.getCourtType());
            cr.save(jpaCourt);

        } catch (Exception ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Iterable<Court> courts = cr.findAll();
        for (Court current : courts) {
            current.setCourtTypeDescription(CodeValueDAO.getCodeValueDescription(new DatabaseConnection(), 2, current.getCourtType()));
        }
        model.addAttribute("courts", courts);

        return "court/list";

    }
    
    @RequestMapping("/court/delete")
    public String delete(Model model, HttpServletRequest request, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }

        //This will send the user to the welcome.html page.
        //CamperDAO.delete(Integer.parseInt(request.getParameter("id")));
        cr.delete(Integer.parseInt(request.getParameter("id")));

        //reload Court type descriptions
        List<Court> courts = (List<Court>) cr.findAll();
        for (Court current : courts) {
            current.setCourtTypeDescription(CodeValueDAO.getCodeValueDescription(new DatabaseConnection(), 2, current.getCourtType()));
        }
        model.addAttribute("courts", courts);

        return "court/list";
    }
    
    @RequestMapping("/court/report")
    public String report(Model model, HttpServletRequest request, HttpSession session, @ModelAttribute("criteria") AvailableCourt criteriaAC){
        //Placeholder ArrayList
        ArrayList<AvailableCourt> availableCourts = new ArrayList<AvailableCourt>();
        
        //Put Test Court into sessions
        AvailableCourt criteria = new AvailableCourt();
            
        availableCourts = CourtBO.findBookableCourts(criteriaAC.getCourtType(),criteriaAC.getStartTime(), criteriaAC.getEndTime(), criteriaAC.getDateBooked());
        
        model.addAttribute("criteria", criteria);
        model.addAttribute("criteriaAC", CourtBO.ValidateCriteria(criteriaAC));
        model.addAttribute("availableCourts",availableCourts);
        return "court/report";
    }
    
    @RequestMapping("/court/service")
    public String service(HttpServletRequest request, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }


        return "court/service";
    }
}
