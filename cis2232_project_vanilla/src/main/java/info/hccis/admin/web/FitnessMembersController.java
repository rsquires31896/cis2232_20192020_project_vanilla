/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.web;

import info.hccis.admin.bo.FitnessMembersBO;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.dao.CourtBookingDAO;
import info.hccis.admin.dao.FitnessMembersDAO;
import info.hccis.admin.data.springdatajpa.FitnessMembersRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.FitnessMember;
import info.hccis.admin.model.entity.FitnessMemberBooking;
import info.hccis.admin.model.entity.FitnessMemberNUser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FitnessMembersController {

    private final FitnessMembersRepository fmr;

    /**
     * This constructor will inject the Repository object into this
     * controller.It will allow this class's methods to use methods from this
     * Spring JPA repository object.
     *
     * @param fmr
     * @since 20191021
     * @author BJM
     * @modified by: asheikho
     * @modification date 20191122
     *
     */
    @Autowired
    public FitnessMembersController(FitnessMembersRepository fmr) {
        this.fmr = fmr;
    }

    /**
     *
     * @author asheikho
     * @param model
     * @param session
     * @throws java.io.IOException
     * @since 20191101
     * @description Exports fitness members to file
     * @return /fitnessMembers/export
     *
     */
    @RequestMapping("/fitnessMembers/export")
    public String exportFitnessMembers(Model model, HttpSession session) throws IOException {

        ArrayList<FitnessMember> fitnessMembers = FitnessMembersDAO.selectAll(); // gets the data from the database
        FitnessMembersBO.writeFitnessMembersToFile(fitnessMembers); // Writes to file

        return "/fitnessMembers/export";
    }

    /**
     *
     * @author asheikho
     * @param model
     * @param session
     * @throws java.io.IOException
     * @since 20191123
     * @description Lists fitness members to web page
     * @return /fitnessMembers/list
     *
     */
    @RequestMapping("/fitnessMembers/list")
    public String listFitnessMembers(Model model, HttpSession session) throws IOException {

        List<FitnessMember> fitnessMembers = (List<FitnessMember>) fmr.findAll();
        model.addAttribute("fitnessMembers", fitnessMembers);

        return "/fitnessMembers/list";
    }

    /**
     *
     * @author asheikho
     * @param model
     * @param session
     * @throws java.io.IOException
     * @since 20191123
     * @description add a new fitness member to database
     * @return /fitnessMembers/add
     *
     */
    @RequestMapping("/fitnessMembers/add")
    public String addFitnessMembers(Model model, HttpSession session) throws IOException {

        FitnessMember fitnessMember = new FitnessMember();
        model.addAttribute("fitnessMember", fitnessMember);

        return "/fitnessMembers/add";
    }

    @RequestMapping("/fitnessMembers/addSubmit")
    public String addSubmit(Model model, HttpSession session, @Valid @ModelAttribute("fitnessMember") FitnessMember fitnessMember, BindingResult result) throws IOException {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);
            return "/fitnessMembers/add";
        }

        try {
            FitnessMember jpaFitnessMember = new FitnessMember();
            jpaFitnessMember.setId(fitnessMember.getId());
            jpaFitnessMember.setUserId(fitnessMember.getUserId());
            jpaFitnessMember.setPhoneCell(fitnessMember.getPhoneCell());
            jpaFitnessMember.setPhoneHome(fitnessMember.getPhoneHome());
            jpaFitnessMember.setPhoneWork(fitnessMember.getPhoneWork());
            jpaFitnessMember.setAddress(fitnessMember.getAddress());
            jpaFitnessMember.setStatus(fitnessMember.getStatus());
            jpaFitnessMember.setMembershipStartDate(fitnessMember.getMembershipStartDate());
            jpaFitnessMember.setMembershipEndDate(fitnessMember.getMembershipEndDate());
            fmr.save(jpaFitnessMember);
        } catch (Exception ex) {
            Logger.getLogger(FitnessMembersController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Iterable<FitnessMember> fitnessMembers = fmr.findAll();
        model.addAttribute("fitnessMembers", fitnessMembers);
        return "/fitnessMembers/list";
    }

    @RequestMapping("/fitnessMembers/delete")
    public String delete(Model model, HttpServletRequest request, HttpSession session) {

//        boolean loggedIn = Utility.verifyLogin(session);
//        //Verify that the user is logged in
//        if (!loggedIn) {
//            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
//            model.addAttribute("user", new UserAccess());
//            return "other/welcome";
//        }
        fmr.delete(Integer.parseInt(request.getParameter("id")));

        List<FitnessMember> fitnessMembers = (List<FitnessMember>) fmr.findAll();
        model.addAttribute("fitnessMembers", fitnessMembers);

        return "/fitnessMembers/list";
    }

    @RequestMapping("/fitnessMembers/edit")
    public String edit(Model model, HttpServletRequest request, HttpSession session) {

        FitnessMembersBO fitnessMembersBO = new FitnessMembersBO(fmr);
        FitnessMember fitnessMember = fitnessMembersBO.getFitnessMember(Integer.parseInt(request.getParameter("id")));
        model.addAttribute("fitnessMember", fitnessMember);

        return "fitnessMembers/add";
    }

    @RequestMapping("/fitnessMembers/report")
    public String reportFitnessMembers(Model model, HttpServletRequest request, HttpSession session, @ModelAttribute("theBooking") FitnessMemberBooking fitnessMemberBooking) throws IOException {

        ArrayList<FitnessMemberBooking> fitnessMemberBookings = FitnessMembersDAO.getFitnessMemberBooking();

        model.addAttribute("fitnessMemberBookings", fitnessMemberBookings);
        return "/fitnessMembers/report";
    }
    
    @RequestMapping("/fitnessMembers/addSubmitReport")
    public String addSubmitReport(Model model, HttpServletRequest request, HttpSession session, @ModelAttribute("theBooking") FitnessMemberBooking fitnessMemberBooking) throws IOException {
    
            FitnessMemberBooking entityFitnessMemberBooking = new FitnessMemberBooking();
            entityFitnessMemberBooking.setUserId(fitnessMemberBooking.getUserId());
            entityFitnessMemberBooking.setStartDate(fitnessMemberBooking.getStartDate());
            entityFitnessMemberBooking.setEndDate(fitnessMemberBooking.getEndDate());
            ArrayList<FitnessMemberBooking> fitnessMemberBookings = FitnessMembersDAO.findFitnessMemberBooking(entityFitnessMemberBooking.getUserId(), entityFitnessMemberBooking.getStartDate(), entityFitnessMemberBooking.getEndDate());
        
        model.addAttribute("fitnessMemberBookings", fitnessMemberBookings);
        return "/fitnessMembers/report";
    }
    


}
