package info.hccis.admin.web.services;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.model.jpa.Court;

import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Services related to courts
 *
 * @author bjmaclean
 * @since Nov 6, 2019
 * @modifiedBy: Robbie Squires
 * @since: Dec 6, 2019
 */
@Path("/CourtService")
public class CourtRest {

    @Resource
    private final CourtRepository cr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * CourtRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     * @modifiedBy: Robbie Squires
     * @since: Dec 6, 2019
     */
    public CourtRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(CourtRepository.class);
    }

    @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("court/{param}")
    public Response getCamper(@PathParam("param") String courtId) {

        String courtJson = "test" + courtId;

        //Camper camper = CamperDAO.select(Integer.parseInt(camperId));
        Court court = cr.findOne(Integer.parseInt(courtId));

        if (court == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(court)).build();
        }

    }

    /**
     * Service to provide all courts
     *
     * @author bjmaclean
     * @return 
     * @since Nov 6, 2019
     * @modifiedBy: Robbie Squires
     * @since: Dec 6, 2019
     */
    @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("/court")
    public Response getCampers() {

        //ArrayList<info.hccis.camper.entity.Camper> campers = CamperDAO.selectAll();
        ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();

        if (courts == null) {
            return Response.status(204).entity("[]").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(courts)).build();
        }

    }

    /**
     * POST operation which will update a court.
     *
     * @param jsonIn
     * @return response including the json representing the new camper.
     * @throws IOException
     * @modifiedBy: Robbie Squires
     * @since: Dec 6, 2019
     */
    @POST
    @Path("/court/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCourt(String jsonIn) throws IOException {

//        ObjectMapper mapper = new ObjectMapper();
        //JSON from String to Object
        Gson gson = new Gson();
        Court court = gson.fromJson(jsonIn, Court.class);
        //Camper camper = mapper.readValue(jsonIn, Camper.class);
        court = cr.save(court);
        String temp = "";
        temp = gson.toJson(court);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

}
