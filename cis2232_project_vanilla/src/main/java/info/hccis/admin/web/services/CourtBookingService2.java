package info.hccis.admin.web.services;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.CourtBookingRepository;
import info.hccis.admin.model.jpa.CourtBooking;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Services related to
 *
 * @author bjmaclean
 * @since Nov 6, 2019
 */
@Path("/BookingService")
public class CourtBookingService2 {

    @Resource
    private final CourtBookingRepository cbr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * CamperRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     */
    public CourtBookingService2(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cbr = applicationContext.getBean(CourtBookingRepository.class);
    }

    @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("/booking/{param}")
    public Response getCourtBooking(@PathParam("param") String id) {

        String courtBookingJson = "test" + id;

        //Camper camper = CamperDAO.select(Integer.parseInt(camperId));
        CourtBooking courtBooking = cbr.findOne(Integer.parseInt(id));

        if (courtBooking == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(courtBooking)).build();
        }

    }

    /**
     * Service to provide all campers
     *
     * @author bjmaclean
     * @since Nov 6, 2019
     */
    @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("/booking")
    public Response getCourtBookings() {

        //ArrayList<info.hccis.camper.entity.Camper> campers = CamperDAO.selectAll();
        ArrayList<CourtBooking> courtBookings = (ArrayList<CourtBooking>) cbr.findAll();

        if (courtBookings == null) {
            return Response.status(204).entity("[]").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(courtBookings)).build();
        }

    }

    /**
     * POST operation which will update a courtbooking.
     *
     * @param jsonIn
     * @return response including the json representing the new courtbooking.
     * @throws IOException
     */
    @POST
    @Path("/booking")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCourtBooking(String jsonIn) throws IOException {

//        ObjectMapper mapper = new ObjectMapper();
        //JSON from String to Object
        Gson gson = new Gson();
        CourtBooking courtBooking = gson.fromJson(jsonIn, CourtBooking.class);
        //Camper camper = mapper.readValue(jsonIn, Camper.class);
        courtBooking = cbr.save(courtBooking);
        String temp = "";
        temp = gson.toJson(courtBooking);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCourtBooking(String jsonIn) throws IOException {

//        ObjectMapper mapper = new ObjectMapper();
        //JSON from String to Object
        Gson gson = new Gson();
        CourtBooking courtBooking = gson.fromJson(jsonIn, CourtBooking.class);
        //Camper camper = mapper.readValue(jsonIn, Camper.class);
        courtBooking = cbr.save(courtBooking);
        String temp = "";
        temp = gson.toJson(courtBooking);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

}
