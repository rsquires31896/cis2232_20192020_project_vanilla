package info.hccis.admin.web.services;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.FitnessMembersRepository;
import info.hccis.admin.model.jpa.FitnessMember;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Services related to FitnessMembers
 *
 * @author bjmaclean
 * @since Nov 6, 2019
 * @modified by asheikho
 * @modification date 20191207
 */
@Path("/FitnessMemberService")
public class FitnessMemberRest {

    @Resource
    private final FitnessMembersRepository cr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * FitnessMemberRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     * @modified by asheikho
     * @modification date 20191207
     */
    public FitnessMemberRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(FitnessMembersRepository.class);
    }

    @GET
    @Path("fitnessmember/{param}")
    public Response getFitnessMember(@PathParam("param") String userId) {

        FitnessMember fitnessMember = cr.findOne(Integer.parseInt(userId));

        if (fitnessMember == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(fitnessMember)).build();
        }

    }

    /**
     * Service to provide all fitnessMembers
     *
     * @author bjmaclean
     * @return
     * @since Nov 6, 2019
     * @modified by asheikho
     * @modification date 20191207
     */
    @GET
    @Path("/fitnessmember")
    public Response getFitnessMembers() {

        ArrayList<FitnessMember> fitnessMembers = (ArrayList<FitnessMember>) cr.findAll();

        if (fitnessMembers == null) {
            return Response.status(204).entity("[]").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(fitnessMembers)).build();
        }

    }

    /**
     * POST operation which will update a fitnessMember.
     *
     * @param jsonIn
     * @return response including the json representing the new fitnessMember.
     * @throws IOException
     */
    @POST
    @Path("/fitnessmember")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateFitnessMember(String jsonIn) throws IOException {

        Gson gson = new Gson();
        info.hccis.admin.model.jpa.FitnessMember fitnessMember = gson.fromJson(jsonIn, info.hccis.admin.model.jpa.FitnessMember.class);
        fitnessMember = cr.save(fitnessMember);
        String temp = "";
        temp = gson.toJson(fitnessMember);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
//
    }

}
