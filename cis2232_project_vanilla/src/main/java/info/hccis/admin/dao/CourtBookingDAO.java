 package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.CourtBooking;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.util.Utility;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.google.gson.Gson;
import info.hccis.admin.model.entity.FitnessMemberNUser;
import info.hccis.admin.model.entity.MemberUsage;
import info.hccis.admin.model.jpa.Court;


/**
 *
 * @author arman
 */

public class CourtBookingDAO {

    /**
     * This will retrieve all courtBookings using databaseConnection parameter
     * @param databaseConnection
     * @return all courtBookings
     */
    public static ArrayList<CourtBooking> getCourtBookings(DatabaseConnection databaseConnection) {
        ArrayList<CourtBooking> courtBookings = new ArrayList<>();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM `CourtBooking` order by bookingDate desc, startTime desc";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CourtBooking courtBooking = new CourtBooking();
                courtBooking.setId(rs.getInt("id"));
                courtBooking.setCourtNumber(rs.getInt("courtNumber"));
                courtBooking.setBookingDate(rs.getString("bookingDate"));
                courtBooking.setStartTime(rs.getString("startTime"));
                courtBooking.setMemberId(rs.getInt("memberId"));
                courtBooking.setMemberIdOpponent(rs.getInt("memberIdOpponent"));
                courtBooking.setNotes(rs.getString("notes"));
                courtBooking.setCreatedDate(rs.getString("createdDate"));
                courtBookings.add(courtBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return courtBookings;
    }
    
    /**
     * will return all CourtBookings without the use of database connection parameter
     * @return all courtBookings
     */
    public static ArrayList<CourtBooking> selectAll() {
        ArrayList<CourtBooking> courtBookings = new ArrayList<>();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

           sql = "SELECT * FROM `CourtBooking` order by bookingDate desc, startTime desc";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CourtBooking courtBooking = new CourtBooking();
                courtBooking.setId(rs.getInt("id"));
                courtBooking.setCourtNumber(rs.getInt("courtNumber"));
                courtBooking.setBookingDate(rs.getString("bookingDate"));
                courtBooking.setStartTime(rs.getString("startTime"));
                courtBooking.setMemberId(rs.getInt("memberId"));
                courtBooking.setMemberIdOpponent(rs.getInt("memberIdOpponent"));
                courtBooking.setNotes(rs.getString("notes"));
                courtBooking.setCreatedDate(rs.getString("createdDate"));
                courtBookings.add(courtBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return courtBookings;
    }

    /**
     * This will insert courtBooking record to the database
     * @param courtBooking record that will be inserted in the database
     * @return success or fail when the insert happens for the record
     * @throws Exception
     */
    public static synchronized String insert(CourtBooking courtBooking) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        String results = "";

        try {
            conn = ConnectionUtils.getConnection();
            String now = Utility.getNow("yyyy-MM-dd HH:mm:ss");

            sql = "INSERT INTO `CourtBooking`(`id`, `courtNumber`, `bookingDate`, startTime, memberId, memberIdOpponent, notes, `createdDate`)"
                    + "VALUES (?,?,?,?,?,?,?,?)";

            if (courtBooking.getId() == null) {
                courtBooking.setId(0);
            }

            ps = conn.prepareStatement(sql);
            ps.setInt(1, courtBooking.getId());
            ps.setInt(2, courtBooking.getCourtNumber());
            ps.setString(3, courtBooking.getBookingDate());
            ps.setString(4, courtBooking.getStartTime());
            ps.setInt(5, courtBooking.getMemberId());
            ps.setInt(6, courtBooking.getMemberIdOpponent());
            ps.setString(7, courtBooking.getNotes());
            ps.setString(8, courtBooking.getCreatedDate());
            ps.execute();
            results = "success";
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            results = "Fail";
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }
        return results;

    }

    /**
     *Purpose is to update the database for the courtBooking record provided
     * @param courtBooking constructor containing the record to update
     * @return courtBooking record that has changed
     * @throws Exception
     */
    public static synchronized CourtBooking update(CourtBooking courtBooking) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            if (courtBooking.getId() == 0) {
                sql = "Insert into `CourtBooking` VALUES (?,?, ?, ?,?, ?,?,?)";
                ps = conn.prepareStatement(sql);
                ps.setInt(1, courtBooking.getId());
                ps.setInt(2, courtBooking.getCourtNumber());
                ps.setString(3, courtBooking.getBookingDate());
                ps.setString(4, courtBooking.getStartTime());
                ps.setInt(5, courtBooking.getMemberId());
                ps.setInt(6, courtBooking.getMemberIdOpponent());
                ps.setString(7, courtBooking.getNotes());
                ps.setString(8, courtBooking.getCreatedDate());

            } else {

                sql = "UPDATE `CourtBooking` SET `courtNumber` = '" + courtBooking.getCourtNumber() 
                        + "', `bookingDate` = '" + courtBooking.getBookingDate()
                        + "', `startTime` = " + courtBooking.getStartTime()+ "', `memberId` = " + courtBooking.getMemberId()
                        + "', memberIdOpponent = " + courtBooking.getMemberIdOpponent() 
                        + "', notes = " + courtBooking.getNotes()
                        + "', `createdDate` = " + courtBooking.getCreatedDate()
                        + " WHERE id = " + courtBooking.getId();
            }
            System.out.println(sql);
            ps.execute();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return courtBooking;
    }

    /**
     * This will delete the record based from id entered
     * @param id record to be deleted
     * @return only return Test value
     * @throws Exception
     */
    public static synchronized String delete(int id) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "DELETE FROM UserAccess WHERE userId = " + id;

            ps = conn.prepareStatement(sql);
            ps.execute();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return "Test";
    }
   
       //will write the full List to the file

    /**
     * This will write the arrayList of CourtBooking into a file
     * @param theList is the records of courtbooking to write to a file
     * @param fileName is the name of the file to write to
     */
    public static void writeListToFile(ArrayList<CourtBooking> theList, String fileName) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(fileName, false);
            bw = new BufferedWriter(fw);

            //loop through the list and write the records to the file
            for (CourtBooking courtBooking : theList) {
                Gson gson = new Gson();
                String jsonString = gson.toJson(courtBooking);

                //Adding line break 
                bw.write(jsonString+System.lineSeparator());
            }
            
            System.out.println("List written to file. Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
    }

    /**
     * This will return the json in gson
     * @return string of the gson
     */
    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
    
    /**
     * This will retrieve the counts of futureBooking based on the memberId
     * @param memberId
     * @return the count of future booking of the member
     */
    public static int getCountFutureBookingByMember(int memberId)
    {
        int recordCount = 0;
        
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT count(*) as counted FROM cis2232_fitness.CourtBooking"
                    + " where (str_to_date(bookingDate,'%Y%m%d') > date(now())" 
                    + " or (str_to_date(bookingDate,'%Y%m%d') =  date(now())) "
                    + " and str_to_date(startTime,'%H%i') > curtime())" 
                    + " and (memberId= ? or memberIdOpponent =?);";
            System.out.println("aa-sql:" + sql);
            ps = conn.prepareStatement(sql);
            ps.setInt(1, memberId);
            ps.setInt(2, memberId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                recordCount = rs.getInt("counted");
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        System.out.println("counted:" + recordCount);
        return recordCount;
    }

    /**
     * This will retrieve all future courtbookings regardless of member
     * @return the list of courtbookings in the future.
     */
    public static ArrayList<CourtBooking> getFutureCourtBookings() {
        ArrayList<CourtBooking> courtBookings = new ArrayList<>();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            //sql = "SELECT * FROM `CourtBooking` order by id";
            sql = "SELECT * FROM cis2232_fitness.CourtBooking "
                    + " where (str_to_date(bookingDate,'%Y%m%d') > date(now())" 
                    + " or (str_to_date(bookingDate,'%Y%m%d') =  date(now())) "
                    + " and str_to_date(startTime,'%H%i') > curtime());" ;
            System.out.println("aa-sql:" + sql);
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CourtBooking courtBooking = new CourtBooking();
                courtBooking.setId(rs.getInt("id"));
                courtBooking.setCourtNumber(rs.getInt("courtNumber"));
                courtBooking.setBookingDate(rs.getString("bookingDate"));
                courtBooking.setStartTime(rs.getString("startTime"));
                courtBooking.setMemberId(rs.getInt("memberId"));
                courtBooking.setMemberIdOpponent(rs.getInt("memberIdOpponent"));
                courtBooking.setNotes(rs.getString("notes"));
                courtBooking.setCreatedDate(rs.getString("createdDate"));
                courtBookings.add(courtBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return courtBookings;
    }
    
    /**
     * This will determine if the court is available based on bookingDate, startTime and courtNumber
     * @param bookingDate
     * @param startTime
     * @param courtNumber
     * @return true or false if the court is available
     */
    public static boolean isCourtAvailable(String bookingDate, String startTime, int courtNumber){
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        int counted = -1;
        
        try {
            conn = ConnectionUtils.getConnection();

            sql = "select count(*) as counted from  CourtBooking where "
                + " bookingDate = ?"
                + " and startTime = ?"
                + " and courtNumber = ?;";
            
            System.out.println("isCourtAvailable sql:" + sql);
            ps = conn.prepareStatement(sql);
            ps.setString(1, bookingDate);
            ps.setString(2, startTime);
            ps.setInt(3,courtNumber);
            System.out.println("preparedStatement:" + ps.toString());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                counted = rs.getInt("counted");
                
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            counted = -1;
        } finally {
            DbUtils.close(ps, conn);
        }
        System.out.println("aa-court availability counter:" + counted);
        return (counted==0);         
        
    }
    
    /**
     * This will return the courtbooking record based on bookingDate, startTime and courtNumber
     * @param bookingDate date of booking
     * @param startTime booking startTime
     * @param courtNumber court identification number
     * @return courtbooking record for the bookingDate, startTime and courtNumber
     * @throws Exception
     */
    public static synchronized ArrayList<CourtBooking> findCourtBookingByCourtNDateAndTime(String bookingDate, 
        String startTime, int courtNumber) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        ArrayList<CourtBooking> courtBookings = new ArrayList<>();
        
        try {
            conn = ConnectionUtils.getConnection();

            sql = "select * from  CourtBooking where "
                 + " bookingDate = ?"
                + " and startTime = ?"
                + " and courtNumber = ?;";
            ps = conn.prepareStatement(sql);
            ps.setString(1, bookingDate);
            ps.setString(2, startTime);
            ps.setInt(3,courtNumber);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CourtBooking courtBooking = new CourtBooking();
                courtBooking.setId(rs.getInt("id"));
                courtBooking.setCourtNumber(rs.getInt("courtNumber"));
                courtBooking.setBookingDate(rs.getString("bookingDate"));
                courtBooking.setStartTime(rs.getString("startTime"));
                courtBooking.setMemberId(rs.getInt("memberId"));
                courtBooking.setMemberIdOpponent(rs.getInt("memberIdOpponent"));
                courtBooking.setNotes(rs.getString("notes"));
                courtBooking.setCreatedDate(rs.getString("createdDate"));
                courtBookings.add(courtBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return courtBookings;         
        
    }

    /**
     * This will retrieve all court bookings based on bookingDate and courtNumber
     * @param bookingDate date of booking
     * @param courtNumber id of the court
     * @return list of courtbookings
     * @throws Exception
     */
    public static synchronized ArrayList<CourtBooking> findCourtBookingByCourtNDate(String bookingDate, 
        int courtNumber) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        ArrayList<CourtBooking> courtBookings = new ArrayList<>();
        
        try {
            conn = ConnectionUtils.getConnection();

            sql = "select * from  CourtBooking where "
                + "bookingDate = ?"
                + "and courtNumber = ?;";
            ps = conn.prepareStatement(sql);
            ps.setString(1, bookingDate);
            ps.setInt(2, courtNumber);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CourtBooking courtBooking = new CourtBooking();
                courtBooking.setId(rs.getInt("id"));
                courtBooking.setCourtNumber(rs.getInt("courtNumber"));
                courtBooking.setBookingDate(rs.getString("bookingDate"));
                courtBooking.setStartTime(rs.getString("startTime"));
                courtBooking.setMemberId(rs.getInt("memberId"));
                courtBooking.setMemberIdOpponent(rs.getInt("memberIdOpponent"));
                courtBooking.setNotes(rs.getString("notes"));
                courtBooking.setCreatedDate(rs.getString("createdDate"));
                courtBookings.add(courtBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return courtBookings;         
        
    }
    
    /**
     * This will retrieve all fitnessMembers with their respective first name and last names
     * @return list of all fitness members
     */
    public static ArrayList<FitnessMemberNUser> getFitnessMemberNUsers() {
        ArrayList<FitnessMemberNUser> fitnessMemberNUsers = new ArrayList<FitnessMemberNUser>();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT ua.userId, fm.id, ua.lastName, ua.firstName FROM cis2232_fitness.FitnessMember fm, cis2232_fitness.UserAccess ua"
                    + " where ua.userid = fm.userId;";                    
            System.out.println("aa-sql:" + sql);
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                FitnessMemberNUser fitnessMemberNUser = new FitnessMemberNUser();
                fitnessMemberNUser.setUserId(rs.getInt("ua.userId"));
                fitnessMemberNUser.setFitnessMemberId(rs.getInt("fm.id"));
                fitnessMemberNUser.setName( rs.getString("ua.firstName") + " " + rs.getString("ua.lastName"));
                System.out.println("name:" + fitnessMemberNUser.getName());
                fitnessMemberNUsers.add(fitnessMemberNUser);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return fitnessMemberNUsers;
    }
    
    /**
     * This will retrieve courtName by searching court number
     * @param courtNumber id of court
     * @return court Name based on courtNumber
     */
    public static String findCourtNameByCourtNumber(int courtNumber){
        
         Court court = new Court();        
     
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM cis2232_fitness.Court ct"
                    + " where ct.courtNumber=?;";                    
            System.out.println("aa-sql:" + sql);
            ps = conn.prepareStatement(sql);
            ps.setInt(1, courtNumber);
            ResultSet rs = ps.executeQuery();
           
            while (rs.next()) {
                
                 court.setId(rs.getInt("ct.id"));
                 court.setCourtName(rs.getString("ct.courtName"));
                 court.setCourtNumber(rs.getInt("ct.courtNumber"));
                 court.setCourtType(rs.getInt("ct.courtType"));
              
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return court.getCourtName();
    } 

    /**
     * This will retrieve the name based on user Id
     * @param userId id of user
     * @return name of the user
     */
    public static String findNameByUserId(int userId){
        
         User user = new User();        
     
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM cis2232_fitness.UserAccess u"
                    + " where u.userId=?;";                    
            System.out.println("aa-sql:" + sql);

            ps = conn.prepareStatement(sql);
            ps.setInt(1,userId);
            ResultSet rs = ps.executeQuery();
           
            while (rs.next()) {
                user.setFirstName(rs.getString("u.firstName"));
                user.setLastName(rs.getString("u.lastName"));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return user.getFirstName() + " " + user.getLastName();
    }

    /**
     * This will count the number of bookings for the member based on start and end date
     * @param memberId id of member
     * @param startDate start date of booking
     * @param endDate end date of booking
     * @return the count of bookings by member Id, start and end date
     */
    public static int getBookingsCountByMemberId(int memberId, int startDate, int endDate){
       
        System.out.println("aa-getBookingsCounterByMember-memberId:" + memberId);

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        try {
            conn = ConnectionUtils.getConnection();

            
            sql = "SELECT count(*) as counted FROM cis2232_fitness.Courtbooking cb"
                    + " where (cb.memberId=?"  
                   + " or cb.memberIdOpponent=?)"
                    + " and str_to_date(cb.bookingDate,'%Y%m%d') >= str_to_date(?,'%Y%m%d')"
                    + " and str_to_date(cb.bookingDate,'%Y%m%d') <= str_to_date(?,'%Y%m%d')"
                    + ";";                    
            System.out.println("aa-sql:" + sql);
            ps = conn.prepareStatement(sql);
            ps.setInt(1, memberId);
            ps.setInt(2, memberId);
            ps.setInt(3, startDate);
            ps.setInt(4, endDate);
            ResultSet rs = ps.executeQuery();
           
            if (rs.next()) {
                
                 return rs.getInt("counted");
              
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return 0;
    }
}
