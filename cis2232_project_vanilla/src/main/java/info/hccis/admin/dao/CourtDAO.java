package info.hccis.admin.dao;

import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.entity.AvailableCourt;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.Court;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rsquires31896
 */
public class CourtDAO {   
    
    public static ArrayList<Court> selectAll(){
        
        ArrayList<Court> courts = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM court order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                int courtNumber = rs.getInt("courtNumber");
                String courtName = rs.getString("courtName");
                int courtType = rs.getInt("courtType");
                courts.add(new Court(id, courtNumber, courtName, courtType));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return courts;
    }
    
    public static ArrayList<AvailableCourt> findByTypeAndTime(int courtType, String startTime, String endTime){
        ArrayList<AvailableCourt> availableCourts = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            sql = "SELECT court.id, court.courtNumber, court.courtName, court.courtType, codeValue.englishDescription , courttimes.startTime, courttimes.endTime\n" +
        "FROM court, courttimes, codeValue\n" +
        "WHERE court.courtType = ?\n" +
        "AND courttimes.startTime >= ?\n" +
        "AND courttimes.endTime <= ?\n" +
        "AND codeValue.codeValueSequence = court.courtType\n"+            
        "ORDER BY courttimes.startTime, court.courtNumber";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, courtType);
            ps.setString(2, startTime);
            ps.setString(3, endTime);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AvailableCourt thisCourt = new AvailableCourt();
                thisCourt.setId(rs.getInt("court.id"));
                thisCourt.setCourtNumber(rs.getInt("court.courtNumber"));
                thisCourt.setCourtName(rs.getString("court.courtName"));
                thisCourt.setCourtType(rs.getInt("court.courtType"));
                thisCourt.setCodeDescription(rs.getString("codeValue.englishDescription"));
                thisCourt.setStartTime(rs.getString("courttimes.startTime"));
                thisCourt.setEndTime(rs.getString("courttimes.endTime"));
                availableCourts.add(thisCourt);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return availableCourts;
    }
    
    public static ArrayList<AvailableCourt> findByTypeTimeAndDate(int courtType, String startTime, String endTime, String Date){
        ArrayList<AvailableCourt> availableCourts = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            sql = "SELECT court.id, courtbooking.courtNumber, court.courtName, court.courtType, codevalue.englishDescription, courtbooking.startTime, courttimes.endTime\n" +
                "FROM courtbooking, court, codevalue, courttimes\n" +
                "WHERE courtbooking.courtNumber = court.courtNumber\n" +
                "AND court.courtType = ?\n" +
                "AND codevalue.codeValueSequence = court.courtType\n" +
                "AND courtbooking.startTime = courttimes.startTime\n" +
                "AND courtbooking.startTime >= ?\n" +
                "AND courttimes.endTime <= ?\n" +
                "AND courtbooking.bookingDate = ?\n" +
                "ORDER BY courtbooking.startTime";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, courtType);
            ps.setString(2, startTime);
            ps.setString(3, endTime);
            ps.setString(4,Date);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AvailableCourt thisCourt = new AvailableCourt();
                thisCourt.setId(rs.getInt("court.id"));
                thisCourt.setCourtNumber(rs.getInt("courtbooking.courtNumber"));
                thisCourt.setCourtName(rs.getString("court.courtName"));
                thisCourt.setCourtType(rs.getInt("court.courtType"));
                thisCourt.setCodeDescription(rs.getString("codeValue.englishDescription"));
                thisCourt.setStartTime(rs.getString("courtbooking.startTime"));
                thisCourt.setEndTime(rs.getString("courttimes.endTime"));
                availableCourts.add(thisCourt);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return availableCourts;
    }
    
}
