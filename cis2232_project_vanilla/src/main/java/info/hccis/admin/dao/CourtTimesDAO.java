package info.hccis.admin.dao;

import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.entity.AvailableCourt;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.model.jpa.CourtTimes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arman
 */

public class CourtTimesDAO {
    
    private String sql = "SELECT courttimes.startTime, courttimes.endTime\n" +
        "FROM courttimes \n" +
        "ORDER BY courttimes.startTime";
    
    public static ArrayList<CourtTimes> selectAll(){
        
        ArrayList<CourtTimes> courtTimes = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            //sql = "SELECT * FROM court order by id";
             sql = "SELECT courttimes.startTime, courttimes.endTime\n" +
                "FROM courttimes \n" +
                "ORDER BY courttimes.startTime";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                String startTime = rs.getString("startTime");
                String endTime = rs.getString("endTime");
               
                courtTimes.add(new CourtTimes(startTime, endTime));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return courtTimes;
    }
    
}
