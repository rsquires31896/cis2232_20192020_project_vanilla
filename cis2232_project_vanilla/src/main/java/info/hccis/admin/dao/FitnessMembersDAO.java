/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.jpa.FitnessMember;
import info.hccis.admin.model.entity.FitnessMemberBooking;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author asheikho
 * @since 20191101
 * @description retrieves table's values from the database
 */
public class FitnessMembersDAO {

    public static ArrayList<FitnessMember> selectAll() {
        ArrayList<FitnessMember> fitnessMembers = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM fitnessmember order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                int userId = rs.getInt("userId");
                String phoneCell = rs.getString("phoneCell");
                String phoneHome = rs.getString("phoneHome");
                String phoneWork = rs.getString("phoneWork");
                String address = rs.getString("address");
                int status = rs.getInt("status");
                fitnessMembers.add(new FitnessMember(id, userId, phoneCell, phoneHome, phoneWork, address, status));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return fitnessMembers;
    }

    /**
     *
     * @author asheikho
     * @since 20191130
     * @description shows fitnessMember bookings using SQL query of UserAccess,
     * FitnessMember, CourtBooking Tables.
     * @return fitnessMemberBooking
     */
    public static ArrayList<FitnessMemberBooking> getFitnessMemberBooking() {

        ArrayList<FitnessMemberBooking> fitnessMemberBooking = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT CONCAT(u.firstName, ' ', u.lastName), c.courtNumber, c.memberId, c.memberIdOpponent, c.startTime, c.notes\n"
                    + "FROM useraccess as u, courtbooking as c\n"
                    + "ORDER BY c.startTime";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                String fullName = rs.getString("CONCAT(u.firstName, ' ', u.lastName)");
                int courtNumber = rs.getInt("c.courtNumber");
                int memberId = rs.getInt("c.memberId");
                int memberIdOpponent = rs.getInt("c.memberIdOpponent");
                String startTime = rs.getString("c.startTime");
                String notes = rs.getString("c.notes");

                fitnessMemberBooking.add(new FitnessMemberBooking(fullName, courtNumber, memberId, memberIdOpponent, startTime, notes));
            }

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return fitnessMemberBooking;

    }

    public static ArrayList<FitnessMemberBooking> findFitnessMemberBooking(int userIdO, String startDate, String endDate) {

        ArrayList<FitnessMemberBooking> fitnessMemberBookings = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT CONCAT(u.firstName, ' ', u.lastName), c.courtNumber, c.memberId, c.memberIdOpponent, c.startTime, c.notes\n"
                    + "FROM useraccess as u, courtbooking as c\n"
                    + "WHERE u.userId = " + userIdO + "\n"
                    + "AND c.bookingDate >= " + startDate + "\n"
                    + "AND c.bookingDate <= " + endDate + "\n"
                    + "ORDER BY c.startTime";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                String fullName = rs.getString("CONCAT(u.firstName, ' ', u.lastName)");
                int courtNumber = rs.getInt("c.courtNumber");
                int memberId = rs.getInt("c.memberId");
                int memberIdOpponent = rs.getInt("c.memberIdOpponent");
                String startTime = rs.getString("c.startTime");
                String notes = rs.getString("c.notes");

                fitnessMemberBookings.add(new FitnessMemberBooking(fullName, courtNumber, memberId, memberIdOpponent, startTime, notes));
            }

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return fitnessMemberBookings;

    }
}
