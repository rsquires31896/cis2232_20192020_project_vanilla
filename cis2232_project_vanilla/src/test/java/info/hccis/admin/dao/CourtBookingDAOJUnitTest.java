/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.dao;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author arman
 */
public class CourtBookingDAOJUnitTest {
    
    public CourtBookingDAOJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testIsCourtAvailableFor20191201at0720Court2ResultTrue() {
        String bookingDate = "20191201"; 
        String startTime = "0720"; 
        int courtNumber = 2;

        boolean result = CourtBookingDAO.isCourtAvailable(bookingDate,  startTime,  courtNumber);
        
        assertSame(result, true);
    }
    
    @Test
    public void testIsCourtAvailableFor20191030at0800Court2ResultFalse() {
        String bookingDate = "20191030"; 
        String startTime = "0800"; 
        int courtNumber = 2;

        boolean result = CourtBookingDAO.isCourtAvailable(bookingDate,  startTime,  courtNumber);
        
        assertSame(result, false);
    }
    
    @Test
    public void testFindCourtNameByCourtNumberForNumber2ResultCourt2(){
        int courtNumber = 2;
        assertEquals(CourtBookingDAO.findCourtNameByCourtNumber(2),"Court 2");
    }
    
    @Test
    public void testFindNameByUserIdFor2ResultJohnSmith(){
        int userId = 2;
        String expected = "John Smith";
        assertEquals(CourtBookingDAO.findNameByUserId(userId),expected);
    }
    
    @Test
    public void testGetBookingsCountByMemberIdFor2AndFutureDateIsZero(){
        int memberId = 2;
        int startDate = 20191201;
        int endDate = 20191231;
        int expected = 0;
        assertEquals(CourtBookingDAO.getBookingsCountByMemberId(memberId, startDate, endDate), expected);
    }
}