## CIS 2232 Project Vanilla

### Group Members

** 1 - Azzam Sheikho **
	
** 2 - Robbie Squires **

** 3 - Arman Alfonso **


### Project Information

Welcome to the CIS2232 project.  

It will require implementation of the basic outcomes of the course.  
The project must work with the environment for the CIS development team.  The system code will adhere 
to CIS standards.  This includes naming and commenting conventions. The project should follow an 
object-oriented design with packages used to organize classes.  These are constraints put on the 
development based on the organizational standards.


Users will have a username and password which will be used to login to the system.  Their username is 
their email address.  The cisadmin application can administer the users of the system.  Login is  a 
requirement but administering the access is not a requirement that needs to be added.
Although most of the requirements are individual, teams must complete some together.  
